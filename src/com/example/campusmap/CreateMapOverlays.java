package com.example.campusmap;

import android.content.Context;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class CreateMapOverlays {
	
	private GoogleMap mMap;
	Context mContext;
	
	public CreateMapOverlays(GoogleMap map, Context context) 
	{
		mMap = map;
		mContext = context;
	}
	
	//Overlay for building list item
	public void createMapOverlay(String name, String lat, String lng, String viccinity) 
	{
		LatLng newPoint = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
		mMap.addMarker(new MarkerOptions()
				.position(newPoint)
				.title("Cal Poly Pomona")
				.snippet(name + ", "+ viccinity)
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.marker)));
				
		CameraPosition cameraPosition = new CameraPosition.Builder()
										    .target(newPoint)      		// Sets the center of the map to new point
										    .zoom(17)                   // Sets the zoom
										    .bearing(180)               // Sets the orientation of the camera to west
										    .tilt(45)                   // Sets the tilt of the camera to 45 degrees
										    .build();                   // Creates a CameraPosition from the builder
		mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		
	}
	
	//Overlay for origin and map touch point
	public void createMapOverlay(LatLng mapPoint, int flag) 
	{
		
		ReverseGeocodeAddress revGeocode = new ReverseGeocodeAddress(mContext);
		
		mMap.addMarker(new MarkerOptions()
				.position(mapPoint)
				.title("Cal Poly Pomona")
				.snippet(flag==1?"Welcome to Cal Poly Pomona":revGeocode.getAddressFromAsynTask(mapPoint))
				.icon(BitmapDescriptorFactory
						.fromResource(flag==1?R.drawable.search:R.drawable.marker)));

		CameraPosition cameraPosition = new CameraPosition.Builder()
										    .target(mapPoint)      		
										    .zoom(17)                  
										    .bearing(180)               
										    .tilt(45)                   
										    .build();                   
		mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		
				
	}

	//Overlay from autocomplete address
	public void createMapOverlay(String address) 
	{
		
		ReverseGeocodeAddress revGeocode = new ReverseGeocodeAddress(mContext);
		LatLng locationPoint = revGeocode.getLatLngFromAddress(address);
		System.out.println(locationPoint.latitude+", "+locationPoint.longitude);
		mMap.addMarker(new MarkerOptions()
				.position(locationPoint)
				.title("Cal Poly Pomona")
				.snippet(address)
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.marker)));
		
		CameraPosition cameraPosition = new CameraPosition.Builder()
										    .target(locationPoint)      
										    .zoom(17)                  
										    .bearing(180)               
										    .tilt(45)                   
										    .build();                   
		mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		
				
	}

}
