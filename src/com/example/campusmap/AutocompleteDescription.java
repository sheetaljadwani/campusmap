package com.example.campusmap;

import java.io.Serializable;

import com.google.api.client.util.Key;

public class AutocompleteDescription implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Key
	public String id;
	
	@Key
	public String description;
	
	

}
