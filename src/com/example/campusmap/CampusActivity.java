package com.example.campusmap;


import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.MapFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import android.os.Bundle;
import android.app.Activity;

import android.content.Intent;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class CampusActivity extends Activity {

	public static final LatLng CALPOLYCAMPUS = new LatLng(34.05932f, -117.81946f);
	private GoogleMap map;
	private static final int REQUESTED_LIST_ITEM = 1;
	private static final int REQUESTED_LOCATION_ITEM = 2;
	
	CreateMapOverlays mOverlay;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_campus);
			
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		
		initMap();
			
	}
	
	public void initMap()
	{
		map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
		map.setMyLocationEnabled(true);
		map.setTrafficEnabled(true);
		map.getUiSettings().setCompassEnabled(true);
		map.getUiSettings().setZoomControlsEnabled(true);
		
		map.setInfoWindowAdapter(new InfoWindowAdapter() {
			
			@Override
			public View getInfoWindow(Marker arg0) {
				return null;
			}
			
			@Override
			public View getInfoContents(Marker point) {
				
				View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
 
               
                String title = point.getTitle();
                String content = point.getSnippet();
 
               TextView tvTitle = (TextView) v.findViewById(R.id.infoTitle);
 
               TextView tvContent = (TextView) v.findViewById(R.id.infoContent);
 
               tvTitle.setText(title);
 
               tvContent.setText(content); 
                
               return v;
 
			}
		});
		
		map.setOnMapLongClickListener(new OnMapLongClickListener() {
			
			@Override
			public void onMapLongClick(LatLng point) {
				mOverlay.createMapOverlay(point,2);
			}
		});
		
		mOverlay = new CreateMapOverlays(map,this);
		mOverlay.createMapOverlay(CALPOLYCAMPUS,1);
	}
	
		
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.list, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch (item.getItemId()) {
		
		case R.id.listButton:
			openListDialog();
			break;
			
		case R.id.search_loc:
			openLocationSearchDialog();
			break;

		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		
		switch (requestCode) 
		{
		case REQUESTED_LOCATION_ITEM:
				if(resultCode == Activity.RESULT_OK )
				{
					if(data != null && data.hasExtra("ADDRESS"))
					{
						String address = data.getStringExtra("ADDRESS");
						mOverlay.createMapOverlay(address);
					}
				}
				else
				{
					Toast.makeText(this, "Failed to get the location", Toast.LENGTH_SHORT).show();
				}
					
			break;
		case REQUESTED_LIST_ITEM:
				if(resultCode == Activity.RESULT_OK )
				{
						if(data != null && data.getAction() =="GET_REF")
						{
							String buildingName =data.getStringExtra("BUILDING_NAME");
							String buildingLat = data.getStringExtra("BUILDING_LAT");	
							String buildingLng = data.getStringExtra("BUILDING_LNG");	
							String buildingVic = data.getStringExtra("BUILDING_VIC");		
							
							mOverlay.createMapOverlay(buildingName, buildingLat, buildingLng, buildingVic);
						}
					
				}
				else
				{
					Toast.makeText(this, "Failed to get the location", Toast.LENGTH_SHORT).show();
				}
				
				break;

		default:
			break;
		}
		
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void openListDialog()
    {
    	Intent listIntent = new Intent(getApplicationContext(), BuildingListActivity.class);
    	startActivityForResult(listIntent,REQUESTED_LIST_ITEM);
    }
	
	private void openLocationSearchDialog()
    {
    	Intent listIntent = new Intent(getBaseContext(), AutocompleteLocationSearch.class);
    	startActivityForResult(listIntent,REQUESTED_LOCATION_ITEM);
    }
	
	public static double getLattitude() {
		return CALPOLYCAMPUS.latitude;
	}

	public static double getLongitude() {
		return CALPOLYCAMPUS.longitude;
	}
	
	
	

}
