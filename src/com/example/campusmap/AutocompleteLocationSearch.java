package com.example.campusmap;

import java.util.ArrayList;
import android.app.Activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;


public class AutocompleteLocationSearch extends Activity{
	
	AutoCompleteTextView autoText;
	Button btnSearch;
	Button btnCancel;
	
	private AutocompleteList resultList;
	private GooglePlacesDataClass googlePlaces;
	private ArrayAdapter<String> adapter;
	private String getAddressForLatLng;
	private ArrayList<String> list = new ArrayList<String>();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.autocomplete_textview_fragment);
		
		autoText = (AutoCompleteTextView) findViewById(R.id.autocomplete_text);
		btnSearch = (Button) findViewById(R.id.btnOk);
		btnCancel =(Button) findViewById(R.id.btnCancel);
		
		btnSearch.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 if(getAddressForLatLng != null && !getAddressForLatLng.isEmpty())
				 {
					 Intent returnIntent = new Intent();
					 returnIntent.putExtra("ADDRESS", getAddressForLatLng);
				     setResult(Activity.RESULT_OK, returnIntent);
				 }
				 else
				 {
					 getAddressForLatLng = ((TextView)findViewById(R.id.autocomplete_text)).getText().toString();
					 System.out.println(getAddressForLatLng);
				 }
					 finish();
			}
		});
		
		btnCancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				 setResult(Activity.RESULT_CANCELED);
			     finish();
			}
		});
		
		
		autoText.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position,long id) {
				getAddressForLatLng = (String) adapterView.getItemAtPosition(position);
			}
		});
		
		autoText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence str, int start, int before, int count) 
			{
				if (count%3 == 1)                    
				     new LoadLocations().execute(str.toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence str, int start, int count,
					int after) {}
					
			@Override
			public void afterTextChanged(Editable str) {}
						
		});
		
	}
	
	
	class LoadLocations extends AsyncTask<String, String, ArrayList<String>>
	{

		@Override
		protected void onPreExecute() 
		{
			 
			super.onPreExecute();
		}
		
		@Override
		protected ArrayList<String> doInBackground(String... params) 
		{
			googlePlaces = new GooglePlacesDataClass();
			
			 try 
			 {
				 resultList = googlePlaces.autocomplete(params[0]);
				
	         } 
			 catch (Exception e) 
			 {
	                e.printStackTrace();
	         }
	         return list;
		
		}
		
		protected void onPostExecute(ArrayList<String> result) 
		{
			
			adapter = new ArrayAdapter<String>(getBaseContext(), R.layout.location_list);
			
			adapter.clear();
			
			autoText.setAdapter(adapter);
			
			if(resultList.predictions.size() > 0)
			{
				 for(AutocompleteDescription desc: resultList.predictions)
				 	 list.add(desc.description);
				 
				 adapter.addAll(list);
			}
 
			adapter.setNotifyOnChange(true);
		}
			
	}

	
}
