package com.example.campusmap;

	import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import org.apache.http.client.HttpResponseException;

import android.util.Log;
import com.google.api.client.googleapis.GoogleHeaders;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonGenerator;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.JsonParser;
import com.google.gson.Gson;

public class GooglePlacesDataClass {

		private static HttpTransport HTTP_TRANSPORT;
		
		private static final String API_KEY = "AIzaSyCRuIpJi3ncEJu2JOlC0l5H3yWGOy3moBA";
		
		// Google Places serach url's
	    private static final String PLACES_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/search/json?";
	    private static final String PLACES_DETAILS_URL = "https://maps.googleapis.com/maps/api/place/details/json?";
	    private static final String PLACES_AUTOCOMPLETE = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";
	  
		private double _lattitude;
		private double _longitude;
		private double _radius;
		
		public GooglePlacesDataClass() {
			
			HTTP_TRANSPORT = new NetHttpTransport();
		}
		
			
		public PlacesList search(double lattitude, double longitude, double radius, String types) 
		{
			this._lattitude = lattitude;
			this._longitude = longitude;
			this._radius = radius;
			
			
			try
			{
				HttpRequestFactory httpRequestFactory = createRequestFactory(HTTP_TRANSPORT);
				HttpRequest request = httpRequestFactory.buildGetRequest(new GenericUrl(PLACES_SEARCH_URL));
				request.getUrl().put("key", API_KEY);
				request.getUrl().put("location", _lattitude+","+ _longitude);
				request.getUrl().put("radius",_radius);
				request.getUrl().put("sensor", true);
				if(types != null)
					request.getUrl().put("types", types);
				
				Gson gson = new Gson();
				
				String json = request.execute().parseAsString();
				PlacesList list = gson.fromJson(json, PlacesList.class);

						
				return list;
			}
			catch(HttpResponseException e)
			{
				Log.e("ERROR", e.getMessage());
				return null;
			} catch (IOException e) {
				Log.e("ERROR", e.getMessage());
				return null;
			}
			
			
			
		}


		public static HttpRequestFactory createRequestFactory(final HttpTransport httpTransport) {
			
			return httpTransport.createRequestFactory( new HttpRequestInitializer() {
				
				@Override
				public void initialize(HttpRequest request) {
					
					GoogleHeaders headers = new GoogleHeaders();
					headers.setApplicationName("Bronco Survival Guide");
					request.setHeaders(headers);

					JsonObjectParser parser = new JsonObjectParser(new JsonFactory() {
						
						@Override
						public JsonParser createJsonParser(InputStream arg0, Charset arg1)
								throws IOException {
							// TODO Auto-generated method stub
							return null;
						}
						
						@Override
						public JsonParser createJsonParser(Reader arg0) throws IOException {
							// TODO Auto-generated method stub
							return null;
						}
						
						@Override
						public JsonParser createJsonParser(String arg0) throws IOException {
							// TODO Auto-generated method stub
							return null;
						}
						
						@Override
						public JsonParser createJsonParser(InputStream arg0) throws IOException {
							// TODO Auto-generated method stub
							return null;
						}
						
						@Override
						public JsonGenerator createJsonGenerator(OutputStream arg0, Charset arg1)
								throws IOException {
							// TODO Auto-generated method stub
							return null;
						}
						
						@Override
						public JsonGenerator createJsonGenerator(Writer arg0) throws IOException {
							// TODO Auto-generated method stub
							return null;
						}
					});
					request.setParser(parser);
					
				}
			});
			
			
		}
		
		
		public PlaceDetails getPlaceDetails(String reference) throws Exception
		{
			try
			{
			
				HttpRequestFactory httpRequestFactory = createRequestFactory(HTTP_TRANSPORT);
				HttpRequest httpRequest = httpRequestFactory.buildGetRequest(new GenericUrl(PLACES_DETAILS_URL));
				httpRequest.getUrl().put("key", API_KEY);
				httpRequest.getUrl().put("reference", reference);
				httpRequest.getUrl().put("sensor","false");
				PlaceDetails place =  httpRequest.execute().parseAs(PlaceDetails.class);
				return place;
			}
			catch (HttpResponseException e) {
				Log.e("Error in performing Details", e.getMessage());
				throw e;
			}
			
		}
		
		
		public AutocompleteList autocomplete(String input) 
		{
		    
		    HttpURLConnection conn = null;
		  
		    try 
		    {
			        StringBuilder sb = new StringBuilder(PLACES_AUTOCOMPLETE);
			        sb.append("sensor=false&key=" + API_KEY);
			        sb.append("&components=country:usa");
			        sb.append("&types=establishment&location="+CampusActivity.getLattitude()+","+CampusActivity.getLongitude());
			        sb.append("&radius=2000");
			        sb.append("&input=" + URLEncoder.encode(input, "utf8"));

			        HttpRequestFactory httpRequestFactory = createRequestFactory(HTTP_TRANSPORT);
					HttpRequest request = httpRequestFactory.buildGetRequest(new GenericUrl(sb.toString()));
										
					Gson gson = new Gson();
					
					String json = request.execute().parseAsString();
										
					AutocompleteList list = gson.fromJson(json, AutocompleteList.class);
								
					return list;

		    } 
		    catch (MalformedURLException e) 
		    {
			        Log.e("ERROR", "Error processing Places API URL", e);
			        return null;
		    } 
		    catch (IOException e) 
		    {
			        Log.e("ERROR", "Error connecting to Places API", e);
			        return null;
		    } 
		    finally 
		    {
			        if (conn != null) 
			            conn.disconnect();
			 }

		}


}


