package com.example.campusmap;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

public class ReverseGeocodeAddress {
	
	private String addressFromLatLng="Cal Poly Pomona";
	private LatLng point;
	private Context mContext;
	
	
	public ReverseGeocodeAddress(Context context) 
	{
		mContext = context;
	}
	
	
	public String getAddress(LatLng mapPoint)
	{
		
		return (addressFromLatLng ==null ?" ":addressFromLatLng );
	}
	
	public void setAddress(String add)
	{
		addressFromLatLng = "";
		addressFromLatLng = add;
	}
	
	public String getAddressFromAsynTask(LatLng mapPoint)
	{
		try 
		{
			addressFromLatLng= new ReverseGeocodingTask(mContext).execute(mapPoint).get();
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		} 
		catch (ExecutionException e) 
		{
			e.printStackTrace();
		}
		return addressFromLatLng;
	}
	
	public LatLng getLatLngFromAddress(String address)
	{
		try 
		{
			point= new GeocodingTask(mContext).execute(address).get();
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		} 
		catch (ExecutionException e) 
		{
			e.printStackTrace();
		}
		return point;
	}
	
	private class ReverseGeocodingTask extends AsyncTask<LatLng, String, String> {
	    Context mContext;
	    String addressText;

	    public ReverseGeocodingTask(Context context) {
	        super();
	        mContext = context;
	    }

	    @Override
	    protected String doInBackground(LatLng... params) 
	    {
	        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

	        List<Address> addresses = null;
	        try 
	        {
	           addresses = geocoder.getFromLocation(params[0].latitude,params[0].longitude, 1);
	           
	        } catch (IOException e) {
	            e.printStackTrace();
	           
	        }
	        if (addresses != null && addresses.size() > 0) 
	        {
	            Address address = addresses.get(0);
	            addressText= String.format("%s, %s, %s",
	                    address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
	                    address.getLocality(),
	                    address.getCountryName());
	         }
	        setAddress(addressText);
	        return addressText;
	    }
	    
	    @Override
	    protected void onPostExecute(String result) 
	    {
	    	super.onPostExecute(result);
	    	setAddress(result);
	    	
	    }
	
	}

		
	    private class GeocodingTask extends AsyncTask<String, LatLng, LatLng> 
	    {
		    Context mContext;
		   
		    public GeocodingTask(Context context) {
		        super();
		        mContext = context;
		    }

		    @Override
		    protected LatLng doInBackground(String... params) 
		    {
		        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
		        LatLng point =null;
		        List<Address> addresses = null;
		        try 
		        {
		           addresses = geocoder.getFromLocationName(params[0], 1);
		           
		        } catch (IOException e) {
		            e.printStackTrace();
		           
		        }
		        if (addresses != null && addresses.size() > 0) 
		        {
		        	point = new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
	            }
		        
		        return point;
		    }
		    
		    @Override
		    protected void onPostExecute(LatLng result) 
		    {
		    	super.onPostExecute(result);
		    }

	    }
	    
	    
	


}
