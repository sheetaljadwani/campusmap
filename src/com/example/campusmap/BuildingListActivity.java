package com.example.campusmap;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class BuildingListActivity extends Activity{
	
	
	private ListView  building_list;
		
	GooglePlacesDataClass googlePlaces = new GooglePlacesDataClass();
	PlacesList nearPlaces;
	
	
    ArrayList<HashMap<String, String>> placesListItems = new ArrayList<HashMap<String,String>>();
 
   
    public static String KEY_REFERENCE = "reference"; 
    public static String KEY_NAME = "name"; 
    public static String KEY_VICINITY = "vicinity"; 
    public static String KEY_LAT = "lattitude"; 
    public static String KEY_LNG = "longitude"; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.bilding_list_activity);
		
		building_list = (ListView) findViewById(R.id.building_list);
		building_list.setOnItemClickListener(mItemListener);
		
		new LoadPlaces().execute();
		
		
	}
	
	public OnItemClickListener mItemListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View v, int arg2,
				long arg3) 
		{
			
			String ref = ((TextView) v.findViewById(R.id.reference)).getText().toString();
			String name = ((TextView) v.findViewById(R.id.name)).getText().toString();
			String lat =((TextView) v.findViewById(R.id.lat)).getText().toString();
			String lng =((TextView) v.findViewById(R.id.lng)).getText().toString();
			String viccinity =((TextView) v.findViewById(R.id.viccinity)).getText().toString();
			
			Intent returnIntent = new Intent(getApplicationContext(),CampusActivity.class);
			
			returnIntent.setAction("GET_REF");
			returnIntent.putExtra("BUILDING_NAME", name);
			returnIntent.putExtra("BUILDING_REF", ref);
			returnIntent.putExtra("BUILDING_LAT", lat);
			returnIntent.putExtra("BUILDING_LNG", lng);
			returnIntent.putExtra("BUILDING_VIC", viccinity);
			
			setResult(RESULT_OK,returnIntent);
			finish();
				
		}
	};
	
	class LoadPlaces extends AsyncTask<String, String, String>
	{

		@Override
		protected String doInBackground(String... params) 
		{
			try {
				 
				 	String types = null; // Listing places only cafes, restaurants
	 
	                // Radius in meters - increase this value if you don't find any places
	                double radius = 1000; // 1000 meters 
	 
	                // get nearest places
	                nearPlaces = googlePlaces.search(CampusActivity.getLattitude(),
	                        CampusActivity.getLongitude(), radius, types);
	 
	            } 
			 catch (Exception e) 
			 {
	                e.printStackTrace();
	         }
	         return null;
		
		}
		
		protected void onPostExecute(String result) {
			
			 // updating UI from Background Thread
	        runOnUiThread(new Runnable() 
	        {
	            public void run() 
	            {
	                /**
	                 * Updating parsed Places into LISTVIEW
	                 * */
	                // Get json response status
	                String status = nearPlaces.status;

	                // Check for all possible status
	                if(status.equals("OK"))
	                {
	                    // Successfully got places details
	                    if (nearPlaces.results != null) 
	                    {
	                        // loop through each place
	                        for (Place p : nearPlaces.results) 
	                        {
	                            HashMap<String, String> map = new HashMap<String, String>();

	                            // Place reference won't display in listview - it will be hidden
	                            // Place reference is used to get "place full details"
	                            map.put(KEY_REFERENCE, p.reference);

	                            // Place name
	                            map.put(KEY_NAME, p.name);
	                            
	                            // Place lattitude
	                            map.put(KEY_LAT, String.valueOf(p.geometry.location.lat));
	                            
	                            //Place longitude
	                            map.put(KEY_LNG, String.valueOf(p.geometry.location.lng));
	                            
	                            //Place viccinity
	                            map.put(KEY_VICINITY, p.vicinity);
	                            
	                            // adding HashMap to ArrayList
	                            placesListItems.add(map);
	                        }
	                        // list adapter
	                        ListAdapter adapter = new SimpleAdapter(BuildingListActivity.this, placesListItems,
	                                R.layout.building_name,
	                                new String[] { KEY_REFERENCE, KEY_NAME,KEY_LAT,KEY_LNG,KEY_VICINITY}, new int[] {
	                                        R.id.reference, R.id.name, R.id.lat, R.id.lng, R.id.viccinity });

	                        // Adding data into listview
	                        building_list.setAdapter(adapter);
	                    }
	                }
	                else if(status.equals("ZERO_RESULTS"))
	                {
	                    // Zero results found
	                    Toast.makeText(getBaseContext(), "No record found", Toast.LENGTH_LONG).show();
	                }
	                else if(status.equals("UNKNOWN_ERROR"))
	                {
	                	 Toast.makeText(getBaseContext(), "Unknown Error", Toast.LENGTH_LONG).show();
	                }
	                else if(status.equals("OVER_QUERY_LIMIT"))
	                {
	                	 Toast.makeText(getBaseContext(), "Over Quality Limit", Toast.LENGTH_LONG).show();
	                }
	                else if(status.equals("REQUEST_DENIED"))
	                {
	                	 Toast.makeText(getBaseContext(), "Request Denied", Toast.LENGTH_LONG).show();
	                }
	                else if(status.equals("INVALID_REQUEST"))
	                {
	                	 Toast.makeText(getBaseContext(), "Invalidate Request", Toast.LENGTH_LONG).show();
	                }
	                else
	                {
	                	 Toast.makeText(getBaseContext(), "Other Errors", Toast.LENGTH_LONG).show();
	                }
	            }
	        });

			
		}
		
		        
	}
    
}