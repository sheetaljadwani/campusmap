package com.example.campusmap;

import android.app.Application;
import android.content.Context;

public class ApplicationContextHandle extends Application {
	
		private static Context mContext;
		
		
		@Override
		public void onCreate() {
			// TODO Auto-generated method stub
			super.onCreate();
			
			mContext = this;
		}
		
		public static Context getAppContext()
		{
			return mContext;
		}

}


